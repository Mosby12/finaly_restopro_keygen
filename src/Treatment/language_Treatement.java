/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Treatment;

/**
 *
 * @author etoum
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class language_Treatement {

    public static String Restaurant_lbl;
    public static String Adress_lbl;
    public static String Mail_lbl;
    public static String Telephone_lbl;
    public static String Recherche_lbl;
    public static String Generator_lbl;
    public static String Setting_lbl;
    public static String Help_lbl;
    public static String Add_lbl_btn;
    public static String Updade_lbl_btn;
    public static String Menu_lbl_btn;
    public static String Delete_lbl_btn;
    public static String Shearch_lbl;
    public static String Reinit_lbl;
    public static String Import_lbl;
    public static String Export_lbl;
    public static String Config_lbl;
    public static String Save_lbl;
    public static String Langue_lbl;
    public static String Restaurant_langue;
    public static String Operation_succes;
    public static String Operation_Failed;
    public static String No_Valid_Date;
    public static String Select_Restaurant;
    public static String GenerateSucces;
    public static String Clique_on;
    public static String Add_operation;
    public static String Update_operation;
    public static String Delete_operation;
    public static String Reset_operation;

    public language_Treatement() {

    }

    public static void AutomatInitiatLangueSave() {
        try {
            //les Codes fichier
            Properties prop = new Properties();
            OutputStream output = null;
            //set the properties values
            output = new FileOutputStream("src/Config/Langue_Fr.dll");
            prop.setProperty("Restaurant_lbl", "Restaurant");
            prop.setProperty("Adress_lbl", "Adresse");
            prop.setProperty("Mail_lbl", "Mail");
            prop.setProperty("Telephone_lbl", "Telephone");
            prop.setProperty("Recherche_lbl", "Recherche");
            prop.setProperty("Generator_lbl", "Generer");
            prop.setProperty("Setting_lbl", "Parametre");
            prop.setProperty("Help_lbl", "Aide?");
            prop.setProperty("Add_lbl_btn", "Ajouter");
            prop.setProperty("Updade_lbl_btn", "Modifier");
            prop.setProperty("Menu_lbl_btn", "Menu");
            prop.setProperty("Delete_lbl_btn", "Supprimer");
            prop.setProperty("Shearch_lbl", "Recherche");
            prop.setProperty("Reinit_lbl", "Reinitialiser");
            prop.setProperty("Import_lbl", "Importer");
            prop.setProperty("Export_lbl", "Exporter");
            prop.setProperty("Config_lbl", "Reglages");
            prop.setProperty("Save_lbl", "Enregister");
            prop.setProperty("Langue_lbl", "Langue");
            prop.setProperty("Operation_succes", "Operation terminer");
            prop.setProperty("Operation_Failed", "Operation non terminer");
            prop.setProperty("No_Valid_Date", "Veillez saisir ou choisir une date valide");
            prop.setProperty("Select_Restaurant", "Veiller selectionner un Restaurant");
            prop.setProperty("GenerateSucces", "Licence Générer avec succès");
            prop.setProperty("Clique_on", "cliquez ici");
            prop.setProperty("Add_operation", "Voulez vous ajouter ces informations?");
            prop.setProperty("Update_operation", "Voulez vous  vraiment modifier ces informations?");
            prop.setProperty("Delete_operation", "Voulez vous vraiment Suppriment ces informations?");
            prop.setProperty("Reset_operation", "Voulez vous effacer toutes les informations enregistrer?");
            //save properties to projet root folder
            prop.store(output, null);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(language_Treatement.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    public static void AutomatConfigSave(String Courant_Language) {
        try {
            //les Codes fichier
            Properties prop = new Properties();
            OutputStream output = null;
            //set the properties values
            output = new FileOutputStream("src/Config/Auto.txt");
            prop.setProperty("Langue", Courant_Language);

            //save properties to projet root folder
            prop.store(output, null);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(language_Treatement.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    public static void AtomatLangueRead(String URL_Langue) throws FileNotFoundException, IOException {
        //les Codes fichier
        Properties prop = new Properties();
        OutputStream output;

        try {
            prop.load(new FileInputStream(URL_Langue));

        } catch (IOException e) {
            AutomatInitiatLangueSave();

        }
        Restaurant_lbl = prop.getProperty("Restaurant_lbl");
        Adress_lbl = prop.getProperty("Adress_lbl");
        Mail_lbl = prop.getProperty("Mail_lbl");
        Telephone_lbl = prop.getProperty("Telephone_lbl");
        Recherche_lbl = prop.getProperty("Recherche_lb");
        Generator_lbl = prop.getProperty("Generator_lbl");
        Setting_lbl = prop.getProperty("Setting_lbl");
        Help_lbl = prop.getProperty("Help_lbl");
        Add_lbl_btn = prop.getProperty("Add_lbl_btn");
        Updade_lbl_btn = prop.getProperty("Updade_lbl_btn");
        Menu_lbl_btn = prop.getProperty("Menu_lbl_btn");
        Delete_lbl_btn = prop.getProperty("Delete_lbl_btn");
        Shearch_lbl = prop.getProperty("Shearch_lbl");
        Reinit_lbl = prop.getProperty("Reinit_lbl");
        Import_lbl = prop.getProperty("Import_lbl");
        Export_lbl = prop.getProperty("Export_lbl");
        Config_lbl = prop.getProperty("Config_lbl");
        Langue_lbl = prop.getProperty("Langue_lbl");
        Save_lbl = prop.getProperty("Save_lbl");
        Operation_succes = prop.getProperty("Operation_succes");
        Operation_Failed = prop.getProperty("Operation_Failed");
        No_Valid_Date = prop.getProperty("No_Valid_Date");
        Select_Restaurant = prop.getProperty("Select_Restaurant");
        GenerateSucces = prop.getProperty("GenerateSucces");
        Clique_on = prop.getProperty("Clique_on");
        Add_operation = prop.getProperty("Add_operation");
        Update_operation = prop.getProperty("Update_operation");
        Delete_operation = prop.getProperty("Delete_operation");
        Reset_operation = prop.getProperty("Reset_operation");

    }

    public static void AtomatLangueConfgRead() throws FileNotFoundException, IOException {
        //les Codes fichier
        Properties prop = new Properties();
        OutputStream output;

        try {
            prop.load(new FileInputStream("src/Config/Auto.txt"));

        } catch (IOException e) {
            AutomatConfigSave("Default");
            conditionLangue();
        }
        Restaurant_langue = prop.getProperty("Langue");
        conditionLangue();
    }

    public static void conditionLangue() throws FileNotFoundException, IOException {
        if (Restaurant_langue.equals("Default")) {
            AtomatLangueRead("src/Config/Lang_En.dll");
        } else if (Restaurant_langue.equals("Francais")) {
            AtomatLangueRead("src/Config/Lang_Fr.dll");

        } else if (Restaurant_langue.equals("Chinois")) {
            AtomatLangueRead("src/Config/Lang_Al.dll");
        } else if (Restaurant_langue.equals("Espagnol")) {
            AtomatLangueRead("src/Config/Lang_Es.dll");
        }

    }

}
