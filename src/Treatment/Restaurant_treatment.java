/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Treatment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.StringTokenizer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author etoum
 */
public class Restaurant_treatment {

    private String RestaurantName, RestoAdress, RestoMail, RestoPhone;
    private static String chemin = "src/Config/database.txt";
    public static ObservableList<Restaurant_treatment> RestoList = FXCollections.observableArrayList();

    public Restaurant_treatment() {
    }

    public void initData(Restaurant_treatment restaurant_treatment) {

    }

    public Restaurant_treatment(String RestaurantName, String RestoAdress, String RestoMail, String RestoPhone) {
        this.RestaurantName = RestaurantName;
        this.RestoAdress = RestoAdress;
        this.RestoMail = RestoMail;
        this.RestoPhone = RestoPhone;
    }

    public Restaurant_treatment(Object RestoListe) {

    }

    public Restaurant_treatment(ObservableList<Restaurant_treatment> Restolist) {
        this.RestaurantName = Restolist.get(0).getRestaurantName();

    }

    @Override
    public String toString() {
        return "Restaurant_treatment{" + "RestaurantName=" + RestaurantName + ", RestoAdress=" + RestoAdress + ", RestoMail=" + RestoMail + ", RestoPhone=" + RestoPhone + '}';
    }

    public String getRestaurantName() {
        return RestaurantName;
    }

    public String getRestoAdress() {
        return RestoAdress;
    }

    public String getRestoMail() {
        return RestoMail;
    }

    public String getRestoPhone() {
        return RestoPhone;
    }

    public void setRestaurantName(String RestaurantName) {
        this.RestaurantName = RestaurantName;
    }

    public void setRestoAdress(String RestoAdress) {
        this.RestoAdress = RestoAdress;
    }

    public void setRestoMail(String RestoMail) {
        this.RestoMail = RestoMail;
    }

    public void setRestoPhone(String RestoPhone) {
        this.RestoPhone = RestoPhone;
    }

    public static void WriteFilePersist(String InfoResto) {
        File file = new File(chemin);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                System.out.println("Erreur: Revoir le fichier notes.txt");
            }
        }
        try {
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(InfoResto);
            bw.newLine();
            bw.close();

        } catch (IOException ex) {
            System.out.println("Erreur: Revoir le fichier notes.txt");
        }

    }

    public void DeleteInformation() {

        Restaurant_treatment restora = null;
        Cleannig_Fonction();
        FileWriter monFichier = null;
        BufferedWriter tampon = null;
        try {
            monFichier = new FileWriter(new File(chemin), true);
            tampon = new BufferedWriter(monFichier);
            //Wrtting objetin the file
            for (int i = 0; i < RestoList.size(); i++) {
                restora = RestoList.get(i);
                tampon.write(restora.getRestaurantName() + "," + restora.getRestoAdress() + "," + restora.getRestoMail() + "," + restora.getRestoPhone() + ";");
                tampon.newLine();
            }

        } catch (IOException exception) {
            exception.printStackTrace();
        } finally {
            try {
                tampon.flush();
                tampon.close();
                monFichier.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

    }

    public static void Cleannig_Fonction() {
        FileWriter fichierSortie = null;

        try {

            fichierSortie = new FileWriter(chemin);
            System.out.println("Le fichier est vider \n");
        } catch (IOException exception) {
            exception.printStackTrace();
        } finally {
            try {
//                    tampon.close(); 
                fichierSortie.close();
            } catch (IOException exception1) {
                exception1.printStackTrace();
            }
        }

    }

    public static void ReadFilePersist() {

        String RestoNam = null;
        String RestoAdress = null;
        String RestoMail = null;
        String RestoPhone = null;
        RestoList.removeAll(RestoList);
        File f = new File(chemin);
        FileWriter fichierSortie = null;
        if (!f.exists()) {
            try {
                System.out.println("Le fichier n'existait pas,je viens de le créer \n");
                fichierSortie = new FileWriter(chemin);//filewriter ccreate file
            } catch (IOException exception) {
                exception.printStackTrace();
            } finally {
                try {
//                    tampon.close(); 
                    fichierSortie.close();
                } catch (IOException exception1) {
                    exception1.printStackTrace();
                }
            }
        } else {
            FileReader monFichier = null;
            BufferedReader tampon = null;
            try {
                monFichier = new FileReader(chemin);
                tampon = new BufferedReader(monFichier);
                System.out.println("Dans la methode lire()");
                while (true) {
                    // Lit une ligne de scores.txt
                    String ligne = tampon.readLine();
                    if (ligne == null) {
                        break;
                    }
                    try {

                        //les séparateurs 
                        StringTokenizer tok = new StringTokenizer(ligne, ";");

                        RestoNam = tok.nextToken(",");
                        RestoAdress = tok.nextToken(",");
                        RestoMail = tok.nextToken(",");
                        RestoPhone = tok.nextToken(",").replace(";", "");
                        RestoList.add(new Restaurant_treatment(RestoNam, RestoAdress, RestoMail, RestoPhone));
                    } catch (Exception e) {
                        Cleannig_Fonction();
                    }

//             Vérification of end of file

//             Vérification of end of file
                } // Fin du while 
            } catch (IOException exception) {
                exception.printStackTrace();
            } finally {
                try {
                    tampon.close();
                    monFichier.close();
                } catch (IOException exception1) {
                    exception1.printStackTrace();
                }
            }
        }
    }

    /**
     * This method will return an ObservableList of People objects
     */
    public static ObservableList<Restaurant_treatment> getResto() {
        return RestoList;

    }

}
