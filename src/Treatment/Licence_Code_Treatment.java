/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Treatment;

import Crypte.AES;
import Crypte.ROT13;
import Crypte.SHA256;
import java.io.BufferedWriter;
import java.io.FileWriter;

/**
 *
 * @author etoum
 */
public class Licence_Code_Treatment {

    public static AES EncrAes = new AES();
    public static ROT13 EncrRot13 = new ROT13();
    public static SHA256 EncrSha256 = new SHA256();
    public static String Key = "DashmakerKeyBar1";
    public static String initVector = "RandomInitVector";
    public static String Verificat_code;
    public static String CryptKey;
    public static String Info_Licence;
    public static String Cryptr_RetoName;
    public static String Cryptr_Finaly_RestoName;
    public static String Cryptr_RestoMail;
    public static String Cryptr_Finaly_RestoMail;
    public static String Cryptr_beguin_Date;
    public static String Cryptr_Finaly_beguin_Date;
    public static String Cryptr_ending_Date;
    public static String Cryptr_Finaly_Ending_Date;

    public static void key_Code_Treatment() {
        CryptKey = ROT13.encrypts(Key);

    }

    public static String finali_InfoResto_Code(String RestoName, String RestoMail, String Begin_date, String ending_date) {
        Cryptr_RetoName = EncrAes.encrypts(CryptKey, initVector, RestoName);
        Cryptr_Finaly_RestoName = ROT13.encrypts(Cryptr_RetoName);
        Cryptr_RestoMail = EncrAes.encrypts(CryptKey, initVector, RestoMail);
        Cryptr_Finaly_RestoMail = ROT13.encrypts(Cryptr_RestoMail);

        Cryptr_beguin_Date = EncrAes.encrypts(CryptKey, initVector, Begin_date);
        Cryptr_Finaly_beguin_Date = ROT13.encrypts(Cryptr_beguin_Date);
        Cryptr_ending_Date = EncrAes.encrypts(CryptKey, initVector, ending_date);
        Cryptr_Finaly_Ending_Date = ROT13.encrypts(Cryptr_ending_Date);

        Verificat_code = Verification_Code(RestoName, RestoMail, Begin_date, ending_date);

        Info_Licence = CryptKey + "\n" + Cryptr_Finaly_RestoName + "\n" + Cryptr_Finaly_RestoMail + "\n" + Cryptr_Finaly_beguin_Date + "\n" + Cryptr_Finaly_Ending_Date + "\n" + Verificat_code;

        return Info_Licence;
    }

    public static String Verification_Code(String RestoName, String RestoMail, String Begin_date, String ending_date) {
        String Info_User_brute = RestoName + RestoMail + Begin_date + ending_date;
        String Verif_info_User = EncrSha256.encrypts(Info_User_brute);

        return Verif_info_User;
    }

    public static void AutomatLiceneSave(String URL) {

        try {
            FileWriter filewrit = new FileWriter(URL);
            BufferedWriter bw = new BufferedWriter(filewrit);
            bw.write("#" + "#IssuedBy = Dashmake Software s.r.o.");
            bw.newLine();
            bw.write("#" + "# Customer number (93-60-41-26)");
            bw.newLine();
            bw.write("LicenceKey = " + CryptKey);
            bw.newLine();
            bw.write("LicenseType = " + "0");
            bw.newLine();
            bw.write("#" + "# Customer name (license holder)");
            bw.newLine();
            bw.write("Resto_Name = " + Cryptr_Finaly_RestoName);
            bw.newLine();
            bw.write("Licence_ID = " + Cryptr_Finaly_RestoMail);
            bw.newLine();
            bw.write("Init_date = " + Cryptr_Finaly_beguin_Date);
            bw.newLine();
            bw.write("Expiry_date = " + Cryptr_Finaly_Ending_Date);
            bw.newLine();
            bw.write("Certificat = " + Verificat_code);

            bw.newLine();
            bw.flush();
            bw.close();
        } catch (Exception e) {
            System.out.println("Battle log failure");
        }
    }

}
