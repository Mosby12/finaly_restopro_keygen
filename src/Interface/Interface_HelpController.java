/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Treatment.language_Treatement;
import static Treatment.language_Treatement.AtomatLangueConfgRead;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author etoum
 */
public class Interface_HelpController implements Initializable {

    @FXML
    private Pane Card_help_id;

    @FXML
    private Label close_btn;
    @FXML
    private Label reduct_btn;
    @FXML
    private Label Menu_btn;
    @FXML
    private Button Generat_btn;
    @FXML
    private Button Restaurant_btn;
    @FXML
    private Button Setting_btn;
    @FXML
    private Button Help_btn;
    @FXML
    private Label Help_lbl;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            Loading_language();
        } catch (IOException ex) {
            Logger.getLogger(Interface_HelpController.class.getName()).log(Level.SEVERE, null, ex);
        }

        //the Setting icon to set on our button
        Image SettingsIcon = new Image(getClass().getResourceAsStream("/Images/settings_icon_128.png"));
        ImageView SettingsIconView = new ImageView(SettingsIcon);
        SettingsIconView.setFitHeight(50);
        SettingsIconView.setFitWidth(50);
        Setting_btn.setGraphic(SettingsIconView);

        //the resto icon to set on our button
        Image RestoIcon = new Image(getClass().getResourceAsStream("/Images/icon-besteck-restaurant.png"));
        ImageView RestoIconView = new ImageView(RestoIcon);
        RestoIconView.setFitHeight(50);
        RestoIconView.setFitWidth(50);
        Restaurant_btn.setGraphic(RestoIconView);

        //the Generat icon to set on our button
        Image GeneratIcon = new Image(getClass().getResourceAsStream("/Images/Generate.png"));
        ImageView GeneratIconView = new ImageView(GeneratIcon);
        GeneratIconView.setFitHeight(50);
        GeneratIconView.setFitWidth(75);
        Generat_btn.setGraphic(GeneratIconView);

        //the helper icon to set on our button
        Image HelpIcon = new Image(getClass().getResourceAsStream("/Images/helpIcon.png"));
        ImageView HelpIconView = new ImageView(HelpIcon);
        HelpIconView.setFitHeight(45);
        HelpIconView.setFitWidth(45);
        Help_btn.setGraphic(HelpIconView);
    }

    @FXML
    private void changeScreenButtonPushedtoRestaurant(ActionEvent event) throws IOException {

        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Interface_Restaurant.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();

    }

    @FXML
    public void changeScreenButtonPushedtoParametre(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Interface_Setting.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    @FXML
    public void changeScreenButtonPushedtoGenerat(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Interface_Generator.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    @FXML
    public void changeScreenButtonPushedtoHelp(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Interface_Help.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    public void Loading_language() throws IOException {
        AtomatLangueConfgRead();

        Menu_btn.setText(language_Treatement.Menu_lbl_btn);
        Generat_btn.setText(language_Treatement.Generator_lbl);
        Restaurant_btn.setText(language_Treatement.Restaurant_lbl);
        Setting_btn.setText(language_Treatement.Setting_lbl);
        Help_lbl.setText(language_Treatement.Help_lbl);
        Help_btn.setText(language_Treatement.Help_lbl);

//     
    }

    @FXML
    public void Close(MouseEvent event) throws IOException {
        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.close();
    }

    @FXML
    public void Reduire(MouseEvent event) throws IOException {
        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setIconified(true);
    }

    @FXML
    public void Deplacement(MouseEvent event) throws IOException {
        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setX(event.getScreenX() + 1);
        window.setY(event.getScreenY() + 1);
    }
}
