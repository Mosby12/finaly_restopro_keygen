/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Treatment.Restaurant_treatment;
import static Treatment.Restaurant_treatment.WriteFilePersist;
import Treatment.language_Treatement;
import static Treatment.language_Treatement.Add_operation;
import static Treatment.language_Treatement.AtomatLangueConfgRead;
import static Treatment.language_Treatement.Select_Restaurant;
import static Treatment.language_Treatement.Update_operation;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author etoum
 */
public class Interface_RestaurantController implements Initializable {

    @FXML
    private Pane Card_Restaurant_id;
    @FXML
    private TableView<Restaurant_treatment> restaurant_tabl;

    @FXML
    private TextField Phone_field;
    @FXML
    private TextField Resto_field;
    @FXML
    private TextField address_field;
    @FXML
    private TextField Mail_field;
    @FXML
    private TableColumn<Restaurant_treatment, String> resto_column;

    @FXML
    private TableColumn<Restaurant_treatment, String> adress_column;

    @FXML
    private TableColumn<Restaurant_treatment, String> mail_column;

    @FXML
    private TableColumn<Restaurant_treatment, String> phone_column;

    @FXML
    private Label Restaurants_lbl;
    @FXML
    private Label Adress_lbl;
    @FXML
    private Label Mail_lbl;
    @FXML
    private Label Phone_lbl;

    @FXML
    private ImageView add_btn;
    @FXML
    private ImageView update_btn;
    @FXML
    private ImageView Delete_btn;
    @FXML
    private Label close_btn;
    @FXML
    private Label reduct_btn;
    @FXML
    private Label Menu_btn;
    @FXML
    private Button Generat_btn;
    @FXML
    private Button Restaurant_btn;
    @FXML
    private Button Setting_btn;
    @FXML
    private Button Help_btn;
    @FXML
    private Label add_Lbl;
    @FXML
    private Label Help_lbl;

    @FXML
    private Label update_Lbl;

    @FXML
    private Label delete_Lbl;

    public static String InfoResto;
    int indice = -1;
    private Restaurant_treatment rest = new Restaurant_treatment();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            Loading_language();
        } catch (IOException ex) {
            Logger.getLogger(Interface_RestaurantController.class.getName()).log(Level.SEVERE, null, ex);
        }
        resto_column.setCellValueFactory(new PropertyValueFactory<Restaurant_treatment, String>("RestaurantName"));
        adress_column.setCellValueFactory(new PropertyValueFactory<Restaurant_treatment, String>("RestoAdress"));
        mail_column.setCellValueFactory(new PropertyValueFactory<Restaurant_treatment, String>("RestoMail"));
        phone_column.setCellValueFactory(new PropertyValueFactory<Restaurant_treatment, String>("RestoPhone"));

        //load dummy data
        Restaurant_treatment.ReadFilePersist();
        restaurant_tabl.setItems(Restaurant_treatment.getResto());
        System.out.println(Restaurant_treatment.getResto());

        //Update the table to allow for the first and last name fields
        //to be editable
        restaurant_tabl.setEditable(true);
        resto_column.setCellFactory(TextFieldTableCell.forTableColumn());
        adress_column.setCellFactory(TextFieldTableCell.forTableColumn());
        mail_column.setCellFactory(TextFieldTableCell.forTableColumn());
        phone_column.setCellFactory(TextFieldTableCell.forTableColumn());

        //This will allow the table to select multiple rows at once
        restaurant_tabl.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        //the Setting icon to set on our button
        Image SettingsIcon = new Image(getClass().getResourceAsStream("/Images/settings_icon_128.png"));
        ImageView SettingsIconView = new ImageView(SettingsIcon);
        SettingsIconView.setFitHeight(50);
        SettingsIconView.setFitWidth(50);
        Setting_btn.setGraphic(SettingsIconView);

        //the resto icon to set on our button
        Image RestoIcon = new Image(getClass().getResourceAsStream("/Images/icon-besteck-restaurant.png"));
        ImageView RestoIconView = new ImageView(RestoIcon);
        RestoIconView.setFitHeight(50);
        RestoIconView.setFitWidth(50);
        Restaurant_btn.setGraphic(RestoIconView);

        //the Generat icon to set on our button
        Image GeneratIcon = new Image(getClass().getResourceAsStream("/Images/Generate.png"));
        ImageView GeneratIconView = new ImageView(GeneratIcon);
        GeneratIconView.setFitHeight(50);
        GeneratIconView.setFitWidth(75);
        Generat_btn.setGraphic(GeneratIconView);

        //the helper icon to set on our button
        Image HelpIcon = new Image(getClass().getResourceAsStream("/Images/helpIcon.png"));
        ImageView HelpIconView = new ImageView(HelpIcon);
        HelpIconView.setFitHeight(45);
        HelpIconView.setFitWidth(45);
        Help_btn.setGraphic(HelpIconView);

    }

    @FXML
    private void changeScreenButtonPushedtoRestaurant(ActionEvent event) throws IOException {

        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Interface_Restaurant.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();

    }

    @FXML
    public void changeScreenButtonPushedtoParametre(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Interface_Setting.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    @FXML
    public void changeScreenButtonPushedtoGenerat(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Interface_Generator.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    @FXML
    public void changeScreenButtonPushedtoHelp(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Interface_Help.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    public void Loading_language() throws IOException {
        AtomatLangueConfgRead();
        Menu_btn.setText(language_Treatement.Menu_lbl_btn);
        Adress_lbl.setText(language_Treatement.Adress_lbl);
        Mail_lbl.setText(language_Treatement.Mail_lbl);
        Phone_lbl.setText(language_Treatement.Telephone_lbl);
        Generat_btn.setText(language_Treatement.Generator_lbl);
        Restaurant_btn.setText(language_Treatement.Restaurant_lbl);
        Setting_btn.setText(language_Treatement.Setting_lbl);
        Restaurants_lbl.setText(language_Treatement.Restaurant_lbl);
        add_Lbl.setText(language_Treatement.Add_lbl_btn);
        Help_lbl.setText(language_Treatement.Help_lbl);
        Help_btn.setText(language_Treatement.Help_lbl);
        delete_Lbl.setText(language_Treatement.Delete_lbl_btn);
        update_Lbl.setText(language_Treatement.Updade_lbl_btn);
        resto_column.setText(language_Treatement.Restaurant_lbl);;
        adress_column.setText(language_Treatement.Adress_lbl);
        mail_column.setText(language_Treatement.Mail_lbl);
        phone_column.setText(language_Treatement.Telephone_lbl);

    }

    public void addResto() {
        if (Resto_field.getText().isEmpty() || address_field.getText().isEmpty() || Mail_field.getText().isEmpty() || Phone_field.getText().isEmpty()) {
            System.out.println(Phone_field.getProperties());
        } else {

            Alert alert = new Alert(AlertType.CONFIRMATION);

            alert.setContentText(Add_operation);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                InfoResto = Resto_field.getText() + "," + address_field.getText() + "," + Mail_field.getText() + "," + Phone_field.getText() + ";";
                // ... user chose OK
                Restaurant_treatment newResto = new Restaurant_treatment(Resto_field.getText(), address_field.getText(),
                        Mail_field.getText(),
                        Phone_field.getText());
                ObservableList< Restaurant_treatment> allResto;
                allResto = restaurant_tabl.getItems();
                allResto.addAll(newResto);
                WriteFilePersist(InfoResto);
                ResetFied();

            } else {
                // ... user chose CANCEL or closed the dialog
            }
        }
    }

    public void deleteResto() {
        ObservableList< Restaurant_treatment> selectedRows, allResto;
        allResto = restaurant_tabl.getItems();
        selectedRows = restaurant_tabl.getSelectionModel().getSelectedItems();

        //loop over the selected rows and remove the Person objects from the table
        if (selectedRows.isEmpty()) {
            Alert alert = new Alert(AlertType.ERROR, Select_Restaurant, ButtonType.OK);
            alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
            alert.show();

        } else {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setHeaderText(language_Treatement.Delete_operation);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                // ... user chose OK
                //this gives us the rows that were selected

                int indice = restaurant_tabl.getSelectionModel().getSelectedIndex();

                allResto.remove(indice);

                rest.DeleteInformation();

            } else {
                // ... user chose CANCEL or closed the dialog
            }
        }
    }

    public void UpdateResto() {
        if (Resto_field.getText().isEmpty() || address_field.getText().isEmpty() || Mail_field.getText().isEmpty() || Phone_field.getText().isEmpty()) {

        } else {
            Restaurant_treatment newResto = new Restaurant_treatment(Resto_field.getText(), address_field.getText(),
                    Mail_field.getText(),
                    Phone_field.getText());
            ObservableList< Restaurant_treatment> selectedRows, allResto;
            allResto = restaurant_tabl.getItems();
            //this gives us the rows that were selected
            selectedRows = restaurant_tabl.getSelectionModel().getSelectedItems();
            //loop over the selected rows and remove the Person objects from the table
            if (selectedRows.isEmpty()) {
                Alert alert = new Alert(AlertType.ERROR, Select_Restaurant, ButtonType.OK);
                alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
                alert.show();

            } else {
                Alert alert = new Alert(AlertType.CONFIRMATION);

                alert.setContentText(Update_operation);

                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    // ... user chose OK

                    //loop over the selected rows and remove the Person objects from the table
                    int indice = restaurant_tabl.getSelectionModel().getSelectedIndex();
                    Restaurant_treatment res = new Restaurant_treatment(Resto_field.getText(), address_field.getText(),
                            Mail_field.getText(),
                            Phone_field.getText());
                    allResto.set(indice, res);
                    rest.DeleteInformation();
                    ResetFied();
                } else {
                    // ... user chose CANCEL or closed the dialog
                }
            }
        }
    }

    public void ResetFied() {
        Phone_field.setText("");
        Resto_field.setText("");
        address_field.setText("");
        Mail_field.setText("");
    }

    @FXML
    public void Close(MouseEvent event) throws IOException {
        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.close();
    }

    @FXML
    public void Reduire(MouseEvent event) throws IOException {
        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setIconified(true);
    }

    @FXML
    public void Deplacement(MouseEvent event) throws IOException {
        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setX(event.getScreenX() + 1);
        window.setY(event.getScreenY() + 1);
    }
}
