/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import static Treatment.Licence_Code_Treatment.AutomatLiceneSave;
import static Treatment.Licence_Code_Treatment.finali_InfoResto_Code;
import static Treatment.Licence_Code_Treatment.key_Code_Treatment;
import Treatment.Restaurant_treatment;
import static Treatment.Restaurant_treatment.WriteFilePersist;
import Treatment.language_Treatement;
import static Treatment.language_Treatement.AtomatLangueConfgRead;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import static Treatment.language_Treatement.GenerateSucces;
import static Treatment.language_Treatement.No_Valid_Date;
import static Treatment.language_Treatement.Select_Restaurant;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author etoum
 */
public class Interface_generateurController implements Initializable {

    @FXML
    private Pane Card_generat_id;

    @FXML
  private TableView<Restaurant_treatment> restaurant_tabl;

    @FXML
    private TableColumn<Restaurant_treatment, String> resto_column;

    @FXML
    private TableColumn<Restaurant_treatment, String> adress_column;

    @FXML
    private TableColumn<Restaurant_treatment, String> mail_column;

    @FXML
    private TableColumn<Restaurant_treatment, String> phone_column;

    @FXML
    private ImageView Generator_btn;

    @FXML
    private Label Generate_lbl;

    @FXML
    private DatePicker Begin_DatePicker;

    @FXML
    private DatePicker EndingDatePicker;

    @FXML
    private Label close_btn;

    @FXML
    private Label CorrectInfo_Labl;

    @FXML
    private Label reduct_btn;

    @FXML
    private Label Menu_btn;

    @FXML
    private Button Generat_btn;

    @FXML
    private Button Restaurant_btn;

    @FXML
    private Button Setting_btn;

    @FXML
    private Button Help_btn;

    @FXML
    private Label Help_lbl;

    @FXML
    private Label beginDateErrorlLbl;

    @FXML
    private Label EndingDateErrorlLb;

    @FXML
    private Pane Movebar;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            Loading_language();
        } catch (IOException ex) {
            Logger.getLogger(Interface_generateurController.class.getName()).log(Level.SEVERE, null, ex);
        }
        resto_column.setCellValueFactory(new PropertyValueFactory<Restaurant_treatment, String>("RestaurantName"));
        adress_column.setCellValueFactory(new PropertyValueFactory<Restaurant_treatment, String>("RestoAdress"));
        mail_column.setCellValueFactory(new PropertyValueFactory<Restaurant_treatment, String>("RestoMail"));
        phone_column.setCellValueFactory(new PropertyValueFactory<Restaurant_treatment, String>("RestoPhone"));
        //load dummy data
        Restaurant_treatment.ReadFilePersist();
        restaurant_tabl.setItems(Restaurant_treatment.getResto());
//        System.out.println(Restaurant_treatment.getResto());

        //Update the table to allow for the first and last name fields
        //to be editable
        restaurant_tabl.setEditable(true);
        resto_column.setCellFactory(TextFieldTableCell.forTableColumn());
        adress_column.setCellFactory(TextFieldTableCell.forTableColumn());
        mail_column.setCellFactory(TextFieldTableCell.forTableColumn());
        phone_column.setCellFactory(TextFieldTableCell.forTableColumn());

        //This will allow the table to select multiple rows at once
        restaurant_tabl.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        //the Setting icon to set on our button
        Image SettingsIcon = new Image(getClass().getResourceAsStream("/Images/settings_icon_128.png"));
        ImageView SettingsIconView = new ImageView(SettingsIcon);
        SettingsIconView.setFitHeight(50);
        SettingsIconView.setFitWidth(50);
        Setting_btn.setGraphic(SettingsIconView);

        //the resto icon to set on our button
        Image RestoIcon = new Image(getClass().getResourceAsStream("/Images/icon-besteck-restaurant.png"));
        ImageView RestoIconView = new ImageView(RestoIcon);
        RestoIconView.setFitHeight(50);
        RestoIconView.setFitWidth(50);
        Restaurant_btn.setGraphic(RestoIconView);

        //the Generat icon to set on our button
        Image GeneratIcon = new Image(getClass().getResourceAsStream("/Images/Generate.png"));
        ImageView GeneratIconView = new ImageView(GeneratIcon);
        GeneratIconView.setFitHeight(50);
        GeneratIconView.setFitWidth(75);
        Generat_btn.setGraphic(GeneratIconView);

        //the helper icon to set on our button
        Image HelpIcon = new Image(getClass().getResourceAsStream("/Images/helpIcon.png"));
        ImageView HelpIconView = new ImageView(HelpIcon);
        HelpIconView.setFitHeight(45);
        HelpIconView.setFitWidth(45);
        Help_btn.setGraphic(HelpIconView);
        
    
         
    }

    @FXML
    private void changeScreenButtonPushedtoRestaurant(ActionEvent event) throws IOException {

        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Interface_Restaurant.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();

    }
    

    @FXML
    public void changeScreenButtonPushedtoParametre(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Interface_Setting.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    @FXML
    public void changeScreenButtonPushedtoGenerat(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Interface_Generator.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    @FXML
    public void changeScreenButtonPushedtoHelp(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Interface_Help.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    public void Loading_language() throws IOException {
        AtomatLangueConfgRead();

        Menu_btn.setText(language_Treatement.Menu_lbl_btn);
        Generate_lbl.setText(language_Treatement.Generator_lbl);
        Generat_btn.setText(language_Treatement.Generator_lbl);
        Restaurant_btn.setText(language_Treatement.Restaurant_lbl);
        Setting_btn.setText(language_Treatement.Setting_lbl);
        Help_lbl.setText(language_Treatement.Help_lbl);
        Help_btn.setText(language_Treatement.Help_lbl);
        resto_column.setText(language_Treatement.Restaurant_lbl);;
        adress_column.setText(language_Treatement.Adress_lbl);
        mail_column.setText(language_Treatement.Mail_lbl);
        phone_column.setText(language_Treatement.Telephone_lbl);
        

    }

    public void Generat() throws IOException {

//                  
        try {

            String BeguinDate = Begin_DatePicker.getValue().toString();
            String EndingDate = EndingDatePicker.getValue().toString();
            if (EndingDatePicker.getValue().isBefore(Begin_DatePicker.getValue())) {

                Alert alert = new Alert(AlertType.INFORMATION, No_Valid_Date, ButtonType.OK);
                alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
                alert.show();
                CorrectInfo_Labl.setText("");

            } else {

                ObservableList< Restaurant_treatment> selectedRows, allResto;
                allResto = restaurant_tabl.getItems();
                //this gives us the rows that were selected
                selectedRows = restaurant_tabl.getSelectionModel().getSelectedItems();
                //loop over the selected rows and remove the Person objects from the table
                if (selectedRows.isEmpty()) {
                    Alert alert = new Alert(AlertType.ERROR, Select_Restaurant, ButtonType.OK);
                    alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
                    alert.show();

                } else {
                    FileChooser fileChooser = new FileChooser();

                    //Set extension filter
                    FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("RestoProLicence files (*.restoprolic)", "*.restoprolic");
                    fileChooser.getExtensionFilters().add(extFilter);
                    Window ownerWindow = null;

                    //Show save file dialog
                    File file = fileChooser.showSaveDialog(ownerWindow);

                    if (file != null) {
                        for (Restaurant_treatment Resto : selectedRows) {
                            String RestoName = Resto.getRestaurantName();
                            String RestoAdress = Resto.getRestoAdress();
                            String RestoMail = Resto.getRestoMail();
                            String Phone = Resto.getRestoPhone();
                            key_Code_Treatment();
                            finali_InfoResto_Code(RestoName, RestoAdress, BeguinDate, EndingDate);
                            AutomatLiceneSave(file.getPath());

                            Alert alert = new Alert(AlertType.INFORMATION, GenerateSucces, ButtonType.OK);
                            alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
                            alert.show();
                            Begin_DatePicker.setValue(null);
                            EndingDatePicker.setValue(null);
                        }
                    }

                }
            }

        } catch (Exception e) {

            CorrectInfo_Labl.setText("");
            System.out.println(e);
            Alert alert = new Alert(AlertType.ERROR, No_Valid_Date, ButtonType.OK);
            alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
            alert.show();
        }

    }

    @FXML
    public void Close(MouseEvent event) throws IOException {
        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.close();
    }

    @FXML
    public void Reduire(MouseEvent event) throws IOException {
        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setIconified(true);
    }

    @FXML
    public void Deplacement(MouseEvent event) throws IOException {
        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setX(event.getScreenX() + 1);
        window.setY(event.getScreenY() + 1);
    }
}
