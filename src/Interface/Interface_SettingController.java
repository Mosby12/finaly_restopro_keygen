/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Treatment.Restaurant_treatment;
import Treatment.language_Treatement;
import static Treatment.language_Treatement.AtomatLangueConfgRead;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import static Treatment.language_Treatement.AutomatConfigSave;
import static Treatment.language_Treatement.Operation_Failed;
import static Treatment.language_Treatement.Operation_succes;
import static Treatment.language_Treatement.Reset_operation;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Optional;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.stage.Window;

/**
 * FXML Controller class
 *
 * @author etoum
 */
public class Interface_SettingController implements Initializable {

    @FXML
    private Pane Card_setting;
    @FXML
    private ImageView Theme_pane;
    @FXML
    private ImageView Langue_pane;
    @FXML
    private ImageView Regl_pane;
    @FXML
    private ImageView Export_Pane;
    @FXML
    private ImageView Pave_pane;
    @FXML
    private ImageView Importe_pane;
    @FXML
    private Label Theme_lbl;
    @FXML
    private Label Langue_lbl;
    @FXML
    private Label Export_lbl;
    @FXML
    private Label Import_lbl;
    @FXML
    private Label close_btn;
    @FXML
    private Label reduct_btn;
    @FXML
    private Label Menu_btn;
    @FXML
    private Button Generat_btn;
    @FXML
    private Button Restaurant_btn;
    @FXML
    private Button Setting_btn;
    @FXML
    private Button Help_btn;
    @FXML
    private Label Help_lbl;
    @FXML
    private ComboBox RoulLangMenu;
     @FXML
    private Button Export_btn;

    @FXML
    private Button Import_btn;

    @FXML
    private Button Reinit_btn;
    private MenuItem menuDefault = new MenuItem("Defaut");
    private MenuItem menuFrancais = new MenuItem("Francais");
    private MenuItem menuChinois = new MenuItem("Chinois");
    private MenuItem menuEspagnol = new MenuItem("Espagnol");

    /**
     * Initializes the controller class.
     */
    @Override

    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            Loading_language();
        } catch (IOException ex) {
            Logger.getLogger(Interface_SettingController.class.getName()).log(Level.SEVERE, null, ex);
        }

        //the Setting icon to set on our button
        Image SettingsIcon = new Image(getClass().getResourceAsStream("/Images/settings_icon_128.png"));
        ImageView SettingsIconView = new ImageView(SettingsIcon);
        SettingsIconView.setFitHeight(50);
        SettingsIconView.setFitWidth(50);
        Setting_btn.setGraphic(SettingsIconView);

        //the resto icon to set on our button
        Image RestoIcon = new Image(getClass().getResourceAsStream("/Images/icon-besteck-restaurant.png"));
        ImageView RestoIconView = new ImageView(RestoIcon);
        RestoIconView.setFitHeight(50);
        RestoIconView.setFitWidth(50);
        Restaurant_btn.setGraphic(RestoIconView);

        //the Generat icon to set on our button
        Image GeneratIcon = new Image(getClass().getResourceAsStream("/Images/Generate.png"));
        ImageView GeneratIconView = new ImageView(GeneratIcon);
        GeneratIconView.setFitHeight(50);
        GeneratIconView.setFitWidth(75);
        Generat_btn.setGraphic(GeneratIconView);

        //the helper icon to set on our button
        Image HelpIcon = new Image(getClass().getResourceAsStream("/Images/helpIcon.png"));
        ImageView HelpIconView = new ImageView(HelpIcon);
        HelpIconView.setFitHeight(45);
        HelpIconView.setFitWidth(45);
        Help_btn.setGraphic(HelpIconView);

        ObservableList<LanguagCompose> list = LanguageDAO.getPlanetList();
        RoulLangMenu.setPromptText(language_Treatement.Langue_lbl);
        RoulLangMenu.setItems(list);

    }

    @FXML
    private void changeScreenButtonPushedtoRestaurant(ActionEvent event) throws IOException {

        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Interface_Restaurant.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();

    }

    @FXML
    public void changeScreenButtonPushedtoParametre(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Interface_Setting.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    @FXML
    public void changeScreenButtonPushedtoGenerat(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Interface_Generator.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    @FXML
    public void changeScreenButtonPushedtoHelp(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Interface_Help.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    public void Loading_language() throws IOException {
        AtomatLangueConfgRead();
        System.out.println();
        Menu_btn.setText(language_Treatement.Menu_lbl_btn);
        Generat_btn.setText(language_Treatement.Generator_lbl);
        Restaurant_btn.setText(language_Treatement.Restaurant_lbl);
        Setting_btn.setText(language_Treatement.Setting_lbl);
        Theme_lbl.setText(language_Treatement.Reinit_lbl);
        Langue_lbl.setText(language_Treatement.Langue_lbl);
        Export_lbl.setText(language_Treatement.Export_lbl);
        Import_lbl.setText(language_Treatement.Import_lbl);
        Help_btn.setText(language_Treatement.Help_lbl);
        Help_lbl.setText(language_Treatement.Help_lbl);
        Reinit_btn.setText(language_Treatement.Clique_on);
        Import_btn.setText(language_Treatement.Clique_on);
        Export_btn.setText(language_Treatement.Clique_on);

    }

    public void Updating_language(ActionEvent event) throws IOException {

        String LangueCouran = (String) RoulLangMenu.getValue().toString();

        if (LangueCouran.equals("English")) {
            AutomatConfigSave("Default");

        } else if (LangueCouran.equals("French")) {
            AutomatConfigSave("Francais");

        } else if (LangueCouran.equals("Espanol")) {
            AutomatConfigSave("Espagnol");

        } else {
            AutomatConfigSave("Default");

        }
        System.out.println(LangueCouran);
        changeScreenButtonPushedtoParametre(event);

    }

    public void resetAppData() {
        Restaurant_treatment.Cleannig_Fonction();
    }

    public boolean ExportFile() {  //Methode permettant la copie d'un fichier 
        boolean resultat = false;
        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("RestoProdb files (*.restoprodb)", "*.restoprodb");
        fileChooser.getExtensionFilters().add(extFilter);
        Window ownerWindow = null;

        //Show save file dialog
        File file = fileChooser.showSaveDialog(ownerWindow);

        if (file != null) {
            File source = new File("src/Config/database.txt");
            File destination = new File(file.getPath());
            // Declaration des flux 
            java.io.FileInputStream sourceFile = null;
            java.io.FileOutputStream destinationFile = null;
            try {
                // Création du fichier : 
                destination.createNewFile();
                // Ouverture des flux 
                sourceFile = new java.io.FileInputStream(source);
                destinationFile = new java.io.FileOutputStream(destination);
                // Lecture par segment de 0.5Mo  
                byte buffer[] = new byte[512 * 1024];
                int nbLecture;
                while ((nbLecture = sourceFile.read(buffer)) != -1) {
                    destinationFile.write(buffer, 0, nbLecture);
                }

                // Copie réussie 
                Infomesage();
                resultat = true;
            } catch (java.io.FileNotFoundException f) {
                System.out.println("Interface.Interface_RestaurantController.fichierPersist()");
                Errormesage();
            } catch (java.io.IOException e) {
                System.out.println("Interface.Interface_RestaurantController.fichierPersist()");
                Errormesage();
            } finally {
                // Quoi qu'il arrive, on ferme les flux 
                try {
                    sourceFile.close();

                } catch (Exception e) {
                }
                try {
                    destinationFile.close();
                } catch (Exception e) {
                }
            }

        } else {
            System.out.println("Interface.Interface_RestaurantController.fichierPersist()");
            Errormesage();
        }
        return (resultat);
    }

    public boolean ImportFile() {  //Methode permettant la copie d'un fichier 
        boolean resultat = false;
        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("RestoProdb files (*.restoprodb)", "*.restoprodb");
        fileChooser.getExtensionFilters().add(extFilter);
        Window ownerWindow = null;

        //Show open file dialog
        File file = fileChooser.showOpenDialog(ownerWindow);

        if (file != null) {
            File source = new File(file.getPath());
            File destination = new File("src/Config/database.txt");
            // flux Declaration  
            java.io.FileInputStream sourceFile = null;
            java.io.FileOutputStream destinationFile = null;
            try {
                // Création  file : 
                destination.createNewFile();
                // Open flux 
                sourceFile = new java.io.FileInputStream(source);
                destinationFile = new java.io.FileOutputStream(destination);
                // read segment: 0.5Mo  
                byte buffer[] = new byte[512 * 1024];
                int nbLecture;
                while ((nbLecture = sourceFile.read(buffer)) != -1) {
                    destinationFile.write(buffer, 0, nbLecture);
                }

                // Copy sucessfuly 
                Infomesage();

            } catch (java.io.FileNotFoundException f) {
            } catch (java.io.IOException e) {
            } finally {
                //flux force close 
                try {
                    sourceFile.close();
                } catch (Exception e) {
                }
                try {
                    destinationFile.close();
                } catch (Exception e) {
                }
            }

        } else {
            System.out.println("Interface.Interface_RestaurantController.fichierPersist()");
            Errormesage();
        }
        return (resultat);
    }

    public void Infomesage() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, Operation_succes, ButtonType.OK);
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.show();

    }

    public void Errormesage() {
        System.out.println("Interface.Interface_RestaurantController.fichierPersist()");
        Alert alert = new Alert(Alert.AlertType.ERROR, Operation_Failed, ButtonType.OK);
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.show();
    }

    @FXML
    public void Close(MouseEvent event) throws IOException {
        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.close();
    }

    @FXML
    public void Reduire(MouseEvent event) throws IOException {
        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setIconified(true);
    }

    @FXML
    public void Deplacement(MouseEvent event) throws IOException {
        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setX(event.getScreenX() + 1);
        window.setY(event.getScreenY() + 1);
    }
}
