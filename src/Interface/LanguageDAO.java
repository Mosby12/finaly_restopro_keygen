/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author etoum
 */
class LanguageDAO {

    public static ObservableList<LanguagCompose> getPlanetList() {
        LanguagCompose English = new LanguagCompose("Anglais", "English");
        LanguagCompose Espanol = new LanguagCompose("Espagnol", "Espanol");
        LanguagCompose French = new LanguagCompose("Francais", "French");

        ObservableList<LanguagCompose> list //
                = FXCollections.observableArrayList(English, French, Espanol);

        return list;
    }

}
