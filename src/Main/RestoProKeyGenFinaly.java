/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import Interface.Interface_generateurController;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/**
 *
 * @author etoum
 */
public class RestoProKeyGenFinaly extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {

        primaryStage = new Stage(StageStyle.UNDECORATED);
        primaryStage.centerOnScreen();
        loadSplashScreen(primaryStage);

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);

    }
//

    private void loadSplashScreen(Stage primaryStage) {
        try {
            //Load splash screen view FXML
            Parent root = FXMLLoader.load(getClass().getResource(("/Interface/SplashScreen.fxml")));
            //Add it to root container (Can be StackPane, AnchorPane etc)
            Scene scene = new Scene(root);

            primaryStage.setScene(scene);
            primaryStage.show();

            //Load splash screen with fade in effect
            FadeTransition fadeIn = new FadeTransition(Duration.seconds(4), root);
            fadeIn.setFromValue(0);
            fadeIn.setToValue(1);
            fadeIn.setCycleCount(1);

            //Finish splash with fade out effect
            FadeTransition fadeOut = new FadeTransition(Duration.seconds(4), root);
            fadeOut.setFromValue(1);
            fadeOut.setToValue(0);
            fadeOut.setCycleCount(1);

            fadeIn.play();

            //After fade in, start fade out
            fadeIn.setOnFinished((e) -> {
                fadeOut.play();

            });

            //After fade out, load actual content
            fadeOut.setOnFinished((e) -> {
                try {
                    Parent parentContent = FXMLLoader.load(getClass().getResource(("/Interface/Interface_Generator.fxml")));
                    scene.setRoot(parentContent);
                    primaryStage.sizeToScene();
                    primaryStage.centerOnScreen();

                } catch (IOException ex) {
                    Logger.getLogger(Interface_generateurController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        } catch (IOException ex) {
            Logger.getLogger(Interface_generateurController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
